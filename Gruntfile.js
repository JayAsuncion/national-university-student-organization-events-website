module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    grunt.cacheMap = [];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            target: {
                options : {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    'src/assets/css/global-styles/global-styles.css' : [
                        'src/assets/less/compiled-global-styles/global-styles.less'
                    ]
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'src/assets/css_min/global-styles/global-styles.min.css' : [
                        'src/assets/css/global-styles/global-styles.css'
                    ]
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            libraries: {
                files: {

                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default',['less', 'uglify', 'cssmin']);
};
