import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SharedModule} from './shared/shared.module';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
    faAddressBook, faBars,
    faArrowRight, faBullseye,
    faCalendar,
    faClock,
    faEnvelope, faEye,
    faHandPointLeft, faMapMarkedAlt,
    faUser,
    faUserEdit
} from '@fortawesome/free-solid-svg-icons';
import {
    faElementor,
    faFacebookF,
    faFacebookSquare,
    faInstagramSquare,
    faTwitter,
    faTwitterSquare
} from '@fortawesome/free-brands-svg-icons';
import {AppRoutes} from './app.routes';
import {HomeModule} from './home/home.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FontAwesomeModule,
        AppRoutes,
        SharedModule,
        HomeModule
    ],
    providers: [],
    bootstrap: [
        AppComponent]
})
export class AppModule {
    constructor(library: FaIconLibrary) {
        // fab = brands, fas = solid, far = regular, fal = light, fad = duo tone
        library.addIcons(faEnvelope, faHandPointLeft, faUser, faClock, faAddressBook, faArrowRight, faBars);
        library.addIcons(faCalendar, faElementor, faUserEdit);
        library.addIcons(faFacebookF, faTwitter);
        // About Us
        library.addIcons(faEye, faBullseye);
        // Contact Us
        library.addIcons(faFacebookSquare, faInstagramSquare, faTwitterSquare, faMapMarkedAlt);
    }
}
