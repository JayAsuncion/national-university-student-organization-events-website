import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AboutUsComponent} from './pages/components/templates/about-us.component';
import {ContactUsComponent} from './pages/components/templates/contact-us.component';
import {NusgStudentCongressComponent} from './pages/components/templates/nusg-student-congress.component';

const routes: Routes = [
    {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
    },
    {
        path: 'about-us',
        component: AboutUsComponent
    },
    {
        path: 'contact-us',
        component: ContactUsComponent
    },
    {
        path: 'nusg-student-congress',
        component: NusgStudentCongressComponent
    },
    {
        path: 'nusg-student-congress/resolutions',
        loadChildren: './resolutions/resolutions.module#ResolutionsModule'
    },
    {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
    },
    {
        path: 'colleges',
        loadChildren: './colleges/colleges.module#CollegesModule'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutes {}
