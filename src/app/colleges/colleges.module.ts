import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {CollegeListComponent} from './components/college-list/college-list.component';
import {CollegeItemComponent} from './components/college-item/college-item.component';
import {CollegesRoutes} from './colleges.routes';
import {CollegesService} from './services/colleges.service';
import {FacebookModule} from 'ngx-facebook';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FontAwesomeModule,
        PdfViewerModule,
        FacebookModule.forRoot(),
        CollegesRoutes
    ],
    declarations: [
        CollegeListComponent,
        CollegeItemComponent
    ],
    exports: [
    ],
    providers: [
        CollegesService
    ]
})
export class CollegesModule {}
