import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CollegeListComponent} from './components/college-list/college-list.component';
import {CollegeItemComponent} from './components/college-item/college-item.component';

const routes: Routes = [
    {
        path: '',
        component: CollegeListComponent,
        data: {}
    },
    {
        path: ':college_id',
        component: CollegeItemComponent,
        data: {}
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CollegesRoutes {}
