import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CollegesService} from '../../services/colleges.service';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {PagesService} from '../../../pages/services/pages.service';
import {FacebookService, InitParams} from 'ngx-facebook';
declare var FB: any;

@Component({
    selector: 'app-college-item',
    templateUrl: './college-item.component.html',
    styleUrls: ['../../../../assets/less/modules/colleges/college-item.less'],
})
export class CollegeItemComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public collegeItemData;
    public isCollegeItemLoaded = false;

    public isCollegeEventsLoaded = false;
    public collegeEvents = [];

    public faceBookContainerWidth = 0;

    constructor(
        private activatedRoute: ActivatedRoute,
        private collegesService: CollegesService,
        private facebookService: FacebookService,
        private pagesService: PagesService,
        private router: Router
    ) {}

    ngOnInit(): void {
        const collegeID = this.activatedRoute.snapshot.paramMap.get('college_id');
        this.collegesService.getCollege(collegeID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeItemData = CollegeItemComponent.processCollegeLogoData(response.data.college);
                        console.log('this.collegeItemData', this.collegeItemData);
                    } else {
                        this.router.navigate(['/colleges']);
                    }

                    this.isCollegeItemLoaded = true;

                    setTimeout(() => {
                        this.initializeFacebookIframe();
                    }, 0);
                },
                error => {
                    this.isCollegeItemLoaded = true;
                }
        );

        this.loadCollegeEvents(collegeID);
    }

    private initializeFacebookIframe() {
        this.faceBookContainerWidth = document.getElementById('facebook_container').offsetWidth;
        let initParams: InitParams = {
            appId: '230831998161583',
            xfbml: true,
            version: 'v2.8'
        };

        this.facebookService.init(initParams);
        const appFacebookLoader = document.getElementById('app_facebook_loader');

        if (appFacebookLoader !== null) {
            FB.XFBML.parse();
        }
    }

    private loadCollegeEvents(collegeID) {
        console.log('loadCollegeEvents');
        const options = {
            page_category_code: 'event_page',
            offset: 0,
            limit: 1000,
            college_id: collegeID,
            date_range: {start_date: '', end_date: ''}
        };
        this.pagesService.getPagesListDetailsAndContent(options)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeEvents = CollegeItemComponent.pageDataDecoder(response.data.pageList);
                    }
                    console.log('collegeEvents', this.collegeEvents);
                    this.isCollegeEventsLoaded = true;
                },
                error => {
                    if (error.error.status === 400) {

                    }

                    this.isCollegeEventsLoaded = true;
                }
        );
    }

    private static processCollegeLogoData(collegeItemData) {
        collegeItemData.college_logo = JSON.parse(collegeItemData.college_logo);
        collegeItemData.college_courses_offered = JSON.parse(collegeItemData.college_courses_offered);
        return collegeItemData;
    }

    private static pageDataDecoder(value) {
        const listCount = value.length;

        for (let i = 0; i < listCount; i++) {
            const pageContentsCount = (value[i].page_contents).length;

            for (let j = 0; j < pageContentsCount; j++) {
                value[i].page_contents[j].json_content = CollegeItemComponent.jsonDecode(
                    value[i].page_contents[j].json_content);
                value[i].page_contents[j].json_content.event_date = CollegeItemComponent.dateDecode(
                    value[i].page_contents[j].json_content.event_date);
            }
        }

        return value;
    }

    static jsonDecode(data) {
        return JSON.parse(data);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    public eventItemClickEvent(pageDetailID, pageName) {
        console.log('clicked');
        localStorage.setItem('current_event', pageName);
        this.router.navigate([`/pages/event_page/${pageDetailID}`]);
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

}
