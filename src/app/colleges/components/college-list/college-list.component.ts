import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CollegesService} from '../../services/colleges.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-college-list',
    templateUrl: './college-list.component.html',
    styleUrls: ['../../../../assets/less/modules/colleges/college-list.less'],
})
export class CollegeListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public isCollegeListLoaded = false;
    public collegeList = [];

    constructor(
        private collegesService: CollegesService
    ) {}

    ngOnInit(): void {
        this.collegesService.getColleges()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        console.log(response.data.colleges);
                        this.collegeList = CollegeListComponent.processCollegeLogoData(response.data.colleges);
                        console.log(this.collegeList);
                    }

                    this.isCollegeListLoaded = true;
                },
                error => {
                    this.isCollegeListLoaded = false;
                }
        )
    }

    private static processCollegeLogoData(collegeListData) {
        const collegeListLength = collegeListData.length;

        for (let i = 0; i < collegeListLength; i++) {
            collegeListData[i].college_logo = JSON.parse(collegeListData[i].college_logo);
        }

        return collegeListData;
    }


    ngOnDestroy(): void {
        this.destroyed$.next();
    }
}
