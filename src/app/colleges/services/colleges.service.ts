import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class CollegesService {
    private resourceUrl = environment.app.api_url + '/colleges';

    constructor(
        private httpClient: HttpClient
    ) {}

    public getColleges(): Observable<any> {
        const url = this.resourceUrl + `?is_college=y`;
        return this.httpClient.get(url);
    }

    public getCollege(collegeID): Observable<any> {
        const url = this.resourceUrl + `/${collegeID}`;
        return this.httpClient.get(url);
    }
}
