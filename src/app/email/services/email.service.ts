import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class EmailService implements OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/emails';

    constructor(
        private httpClient: HttpClient
    ) {}


    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    submitEmail(formData): Observable<any> {
        return this.httpClient.post(this.resourceUrl, formData);
    }
}
