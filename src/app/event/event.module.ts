import {NgModule} from '@angular/core';
import {EventCategoryService} from './services/event-category.service';

@NgModule({
    imports: [
    ],
    declarations: [
    ],
    exports: [
    ],
    providers: [
        EventCategoryService
    ]
})
export class EventModule {}
