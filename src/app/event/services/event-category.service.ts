import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class EventCategoryService implements OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    getAllEventCategories(): Observable<any> {
        const url = this.resourceUrl + '/event-category';
        return this.httpClient.get(url);
    }
}
