import {AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {PagesService} from '../../pages/services/pages.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {FacebookService, InitParams} from 'ngx-facebook';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmailService} from '../../email/services/email.service';
import {HomeService} from "../services/home.service";
declare var FB: any;

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['../../../assets/less/modules/home/home.less'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public homeBannerItems = [];
    public latestEvents = [];
    public upcomingEvents = [];
    public sponsorsList = [];
    public faceBookContainerWidth = 0;

    public grievanceForm: FormGroup;
    public isGrievanceFormSubmitted = false;
    public grievanceFormState = '';

    static getTomorrowDateTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + (today.getDate() + 1)).slice(-2);
        const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        // return date + ' ' + time;
        return date;
    }

    static jsonDecode(data) {
        return JSON.parse(data);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    static pageDataDecoder(value) {
        const listCount = value.length;

        for (let i = 0; i < listCount; i++) {
            const pageContentsCount = (value[i].page_contents).length;

            for (let j = 0; j < pageContentsCount; j++) {
                value[i].page_contents[j].json_content = HomeComponent.jsonDecode(
                    value[i].page_contents[j].json_content);
                value[i].page_contents[j].json_content.event_date = HomeComponent.dateDecode(
                    value[i].page_contents[j].json_content.event_date);
            }
        }

        return value;
    }

    constructor(
        private emailService: EmailService,
        private facebookService: FacebookService,
        private homeService: HomeService,
        private pageService: PagesService
    ) {}

    ngOnInit(): void {
        this.loadHomeBannerItems();
        this.faceBookContainerWidth = document.getElementById('facebook_container').offsetWidth;
        let initParams: InitParams = {
            appId: '230831998161583',
            xfbml: true,
            version: 'v2.8'
        };
        this.facebookService.init(initParams);
        const appFacebookLoader = document.getElementById('app_facebook_loader');

        if (appFacebookLoader !== null) {
            FB.XFBML.parse();
        }

        this.pageService.getPagesListDetails().pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.latestEvents = response.data.pageList;
                }
            },
            error => {
                console.log('getPagesListDetails', error);
            }
        );

        this.loadUpcomingEvents();
        this.loadAllEvents();

        this.buildGrievanceForm();
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private loadHomeBannerItems() {
        this.homeService.getHomeBannerItems()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.homeBannerItems = response.data.home_carousel_item_list;
                    }
                },
                error => {

                }
        );
    }

    private loadUpcomingEvents() {
        // Get Upcoming
        const tomorrowDateTime = HomeComponent.getTomorrowDateTime();
        this.pageService.getPagesListDetailsAndContent({offset: 0, limit: 2, date_range: {start_date: tomorrowDateTime, end_date: ''}})
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                this.upcomingEvents = HomeComponent.pageDataDecoder(response.data.pageList);
            },
            error => {
                console.log('getPagesListDetails Upcoming Events', error);
            }
        );
    }

    private loadAllEvents() {
        this.pageService.getPagesListDetailsAndContent({offset: 0, limit: 100, date_range: {}})
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                const allEvents = HomeComponent.pageDataDecoder(response.data.pageList);
                this.sponsorsList = HomeComponent.retrieveSponsorsFromEvents(allEvents);
            },
            error => {
                console.log('getPagesListDetails Upcoming Events', error);
            }
        );
    }

    private static retrieveSponsorsFromEvents(eventsData) {
        const eventsDataLength = eventsData.length;
        let retrievedSponsorsImages = [];

        for (let i = 0; i < eventsDataLength; i++) {
            let eventSponsors = eventsData[i].page_contents[0].json_content.sponsors_partners;

            if (eventSponsors !== undefined && Array.isArray(eventSponsors)) {
                let imagesLength = eventSponsors.length;

                for (let j = 0; j < imagesLength; j++) {
                    retrievedSponsorsImages.push({url: eventSponsors[j].url});
                }
            }
        }

        return retrievedSponsorsImages;
    }

    private buildGrievanceForm() {
        this.grievanceForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            subject: new FormControl('', [Validators.required]),
            concern: new FormControl('', [Validators.required]),
            suggestion: new FormControl('', [Validators.required])
        });
    }

    submitGrievanceForm() {
        if (this.grievanceFormState === 'Sending') {
            return;
        }

        this.isGrievanceFormSubmitted = true;

        if (!this.grievanceForm.valid) {
            console.log('not valid');
            return;
        }

        this.grievanceFormState = 'Sending';
        let formData = this.grievanceForm.value;
        formData.source_form = 'GRIEVANCE_FORM';

        this.emailService.submitEmail(formData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.grievanceForm.reset();

                        this.isGrievanceFormSubmitted = false;
                        this.grievanceFormState = 'Sent';
                    } else {
                        this.grievanceFormState = 'Error';
                    }
                },
                error => {
                    console.log('submitEmail', error);
                }
        );

    }

    get gf() {
        return this.grievanceForm.controls;
    }
}
