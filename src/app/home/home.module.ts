import {NgModule} from '@angular/core';
import {HomeRoutes} from './home.routes';
import {HomeComponent} from './components/home.component';
import {SharedModule} from '../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {PagesModule} from '../pages/pages.module';
import {FacebookModule} from 'ngx-facebook';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        HomeRoutes,
        FontAwesomeModule,
        SharedModule,
        PagesModule,
        CarouselModule,
        FacebookModule.forRoot(),
        ReactiveFormsModule,
        CommonModule
    ],
    declarations: [
        HomeComponent
    ],
    exports: [
        HomeComponent
    ]
})
export class HomeModule {}
