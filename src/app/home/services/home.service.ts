import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class HomeService implements OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/home-carousel-items';

    constructor(
        private httpClient: HttpClient
    ) {
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    getHomeBannerItems(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }
}
