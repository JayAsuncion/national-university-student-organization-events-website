import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PagesService} from '../services/pages.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-page-item',
    templateUrl: './page-item.component.html',
    styleUrls: ['../../../assets/less/modules/pages/page-item.less']
})
export class PageItemComponent implements OnInit {
    private destroyed$ = new Subject();
    public isPageLoaded = false;
    public pageCategoryCode;
    public pageName: string;
    public pageDetails;

    public showSidebar = true;

    constructor(
        private activatedRoute: ActivatedRoute,
        private pagesService: PagesService,
        private sanitizer: DomSanitizer
    ) {}

    static jsonDecode(jsonData) {
        return JSON.parse(jsonData);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    ngOnInit(): void {
        this.pageCategoryCode = this.activatedRoute.snapshot.paramMap.get('page_category_code');

        if (this.pageCategoryCode === 'congress_session_page') {
            this.pageName = localStorage.getItem('current_congress_session');
            localStorage.removeItem('current_congress_session');
            this.showSidebar = false;
        } else if (this.pageCategoryCode === 'event_page') {
            this.pageName = localStorage.getItem('current_event');
            localStorage.removeItem('current_event');
            this.showSidebar = true;
        }

        const id = this.activatedRoute.snapshot.paramMap.get('id');
        this.loadPageItem(id);
    }

    public loadPageItem(id) {
        this.isPageLoaded = false;
        this.pagesService.getPagesDetailsAndContent(id).pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.pageDetails = response.data.page_details;
                        this.pageDetails.json_content = PageItemComponent.jsonDecode(this.pageDetails.json_content);
                        this.pageDetails.json_content.event_date = PageItemComponent.dateDecode(this.pageDetails.json_content.event_date);
                        this.pageName = this.pageDetails.page_name;
                    } else {
                        window.open(environment.app.site_url, '_self');
                    }

                    this.isPageLoaded = true;
                },
                error => {
                    console.log('getPagesDetailsAndContent', error);
                }
            );
    }
}
