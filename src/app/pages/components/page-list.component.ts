import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {PagesService} from '../services/pages.service';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {EventCategoryService} from '../../event/services/event-category.service';
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from '@angular/router';

@Component({
    selector: 'app-page-list',
    templateUrl: './page-list.component.html',
    styleUrls: ['../../../assets/less/modules/pages/page-list.less']
})
export class PageListComponent implements OnInit, OnDestroy {

    constructor(
        private activatedRoute: ActivatedRoute,
        private eventCategoryService: EventCategoryService,
        private pagesService: PagesService,
        private router: Router
    ) {}

    set latestEvent(value) {
        this.latestEventsData = PageListComponent.pageDataDecoder(value);
    }

    set upcomingEvent(value) {
        this.upcomingEventsData = PageListComponent.pageDataDecoder(value);
    }

    private destroyed$ = new Subject();

    public pageCategoryCode;
    public eventCategoryID;
    public eventCategoryName = 'Our Latest Events';
    public currentPageNumber = 1;
    public itemsPerPage = 6;
    public totalNumberOfPages = 0;

    public latestEventsData;
    public upcomingEventsData;
    public eventCategories;

    public error404flag = false;
    public comingSoonFlag = false;
    public showSidebar = true;
    public showPagination = true;

    static jsonDecode(data) {
        return JSON.parse(data);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    static getCurrentDateTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
        const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        return date + ' ' + time;
    }

    static getTomorrowDateTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + (today.getDate() + 1)).slice(-2);
        const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        // return date + ' ' + time;
        return date;
    }

    static isNumber(value) {
        return !isNaN(value);
    }

    static pageDataDecoder(value) {
        const listCount = value.length;

        for (let i = 0; i < listCount; i++) {
            const pageContentsCount = (value[i].page_contents).length;

            for (let j = 0; j < pageContentsCount; j++) {
                value[i].page_contents[j].json_content = PageListComponent.jsonDecode(
                    value[i].page_contents[j].json_content);
                value[i].page_contents[j].json_content.event_date = PageListComponent.dateDecode(
                    value[i].page_contents[j].json_content.event_date);
            }
        }

        return value;
    }

    ngOnInit(): void {
        this.pageCategoryCode = this.activatedRoute.snapshot.paramMap.get('page_category_code');
        if (this.pageCategoryCode === 'congress_session_page') {
            this.eventCategoryName = 'NUSG Congress Sessions';
            this.showSidebar = false;
            this.showPagination = false;
        }

        const pageNumberFromRoute = this.activatedRoute.snapshot.queryParamMap.get('page');
        this.eventCategoryID = this.activatedRoute.snapshot.queryParamMap.get('event_category_id');
        this.eventCategoryID = (this.eventCategoryID !== null) ? this.eventCategoryID : 0;

        this.loadLatestEvents(this.pageCategoryCode, this.eventCategoryID, pageNumberFromRoute);

        this.router.events.pipe(
            filter((event: RouterEvent) => event instanceof NavigationEnd)
        ).subscribe(() => {
            const pageNumberQueryParam = this.activatedRoute.snapshot.queryParamMap.get('page');

            this.pageCategoryCode = this.activatedRoute.snapshot.paramMap.get('page_category_code');
            console.log('navigate', this.pageCategoryCode);

            if (this.pageCategoryCode === 'event_page') {
                this.latestEventsData = undefined;
                this.upcomingEventsData = undefined;
                this.loadLatestEvents(this.pageCategoryCode, this.eventCategoryID, pageNumberFromRoute);
                this.showSidebar = true;
                this.showPagination = true;
            }

            if (pageNumberQueryParam == null) {
                console.log('pageNumberQueryParam is nul');
                this.error404flag = false;
                this.loadLatestEvents(this.pageCategoryCode, this.eventCategoryID, 1);
            }
        });

        // Get Upcoming
        const tomorrowDateTime = PageListComponent.getTomorrowDateTime();
        this.pagesService.getPagesListDetailsAndContent({offset: 0, limit: 2, date_range: {start_date: tomorrowDateTime, end_date: ''}})
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    this.upcomingEvent = response.data.pageList;
                },
            error => {
                console.log('getPagesListDetails Upcoming Events', error);
            }
        );

        this.eventCategoryService.getAllEventCategories()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    this.eventCategories = response.data.event_category_list;
                    for (const eventCategory of this.eventCategories) {
                        if (this.eventCategoryID == eventCategory.event_category_id) {
                            this.eventCategoryName = eventCategory.event_category_name;
                        }
                    }
                },
            error => {
                    console.log('getAllEventCategories', error);
            }
        );
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private loadLatestEvents(pageCategoryCode, eventCategoryID, pageNumber) {
        if (this.error404flag) {
            // DO NOT REQUEST ANYMORE SINCE INITIAL AND SUGGESTION REQUESTS ARE ALREADY 404
            this.comingSoonFlag = true;
            return;
        }

        const currentDateTime = PageListComponent.getTomorrowDateTime();
        let pageOffset = 0;

        if (pageNumber !== undefined && PageListComponent.isNumber(pageNumber) && pageNumber > 0) {
            pageOffset = (pageNumber - 1) * 5;
        }

        this.currentPageNumber = parseInt(pageNumber, 10);

        this.pagesService.getPagesListDetailsAndContent({
            page_category_code: pageCategoryCode,
            event_category_id: eventCategoryID,
            offset: pageOffset, limit: 6,
            date_range: {start_date: '', end_date: currentDateTime}})
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.totalNumberOfPages = parseInt(response.data.pagesCount, 10);
                    this.latestEvent = response.data.pageList;
                },
                error => {
                    this.latestEventsData = undefined;
                    this.error404flag = true;
                    this.loadLatestEvents(pageCategoryCode, eventCategoryID, 1);
                }
            );
    }

    pageChangeEvent(event) {
        const pageNumber = event.page;

        if (pageNumber !== this.currentPageNumber) {
            this.router.navigate([], {
                relativeTo: this.activatedRoute,
                queryParams: {
                    page: pageNumber
                },
                queryParamsHandling: 'merge',
                skipLocationChange: false
            });
            this.error404flag = false;
            this.latestEventsData = undefined;
            this.loadLatestEvents(this.pageCategoryCode, this.eventCategoryID, pageNumber);
        }
    }

    eventCategoryClick(eventCategoryID, eventCategoryName) {
        if (eventCategoryID === 0) {
            this.router.navigate([], {
                relativeTo: this.activatedRoute,
                queryParams: {
                    page: 1
                },
                skipLocationChange: false
            });
        } else {
            this.router.navigate([], {
                relativeTo: this.activatedRoute,
                queryParams: {
                    page: 1,
                    event_category_id: eventCategoryID
                },
                queryParamsHandling: 'merge',
                skipLocationChange: false
            });
        }

        this.error404flag = false;
        this.eventCategoryName = eventCategoryName;
        this.latestEventsData = undefined;
        this.loadLatestEvents(this.pageCategoryCode, eventCategoryID, 1);
    }

    pageItemClickEvent(pageDetailID, pageName) {
        localStorage.setItem('current_event', pageName);
        this.router.navigate([`/pages/event_page/${pageDetailID}`]);
    }

    congressSessionItemClickEvent(pageDetailID, pageName) {
        localStorage.setItem('current_congress_session', pageName);
        this.router.navigate([`/pages/congress_session_page/${pageDetailID}`]);
    }

    test() {
        this.currentPageNumber = 2;
    }
}
