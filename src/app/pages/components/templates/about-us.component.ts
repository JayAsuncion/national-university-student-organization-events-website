import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {PagesService} from '../../services/pages.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-page-template-about-us',
    templateUrl: './about-us.component.html',
    styleUrls: ['../../../../assets/less/modules/pages/about-us.less'],
    encapsulation: ViewEncapsulation.None
})
export class AboutUsComponent implements OnInit {
    private destroyed$ = new Subject();
    public isPageLoaded = false;
    public pageDetails;

    static jsonDecode(jsonData) {
        return JSON.parse(jsonData);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    constructor(
        private pagesService: PagesService
    ) {}

    ngOnInit(): void {
        this.pagesService.getPagesDetailsAndContent(12).pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.pageDetails = response.data.page_details;
                    this.pageDetails.json_content = AboutUsComponent.jsonDecode(this.pageDetails.json_content);
                    this.isPageLoaded = true;
                },
                error => {
                    console.log('getPagesDetailsAndContent', error);
                }
            );
    }
}
