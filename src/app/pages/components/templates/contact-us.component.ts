import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {PagesService} from '../../services/pages.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-page-template-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['../../../../assets/less/modules/pages/contact-us.less'],
    encapsulation: ViewEncapsulation.None
})
export class ContactUsComponent implements OnInit {
    private destroyed$ = new Subject();
    public isPageLoaded = false;
    public pageDetails;

    static jsonDecode(jsonData) {
        return JSON.parse(jsonData);
    }

    constructor(
        private pagesService: PagesService
    ) {}

    ngOnInit(): void {
        this.pagesService.getPagesDetailsAndContent(13).pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.pageDetails = response.data.page_details;
                    this.pageDetails.json_content = ContactUsComponent.jsonDecode(this.pageDetails.json_content);
                    console.log(this.pageDetails);
                    this.isPageLoaded = true;
                },
                error => {
                    console.log('getPagesDetailsAndContent', error);
                }
            );
    }
}
