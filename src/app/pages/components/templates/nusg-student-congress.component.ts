import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {PagesService} from '../../services/pages.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-page-template-contact-us',
    templateUrl: './nusg-student-congress.component.html',
    styleUrls: ['../../../../assets/less/modules/pages/nusg-student-congress.less'],
    encapsulation: ViewEncapsulation.None
})
export class NusgStudentCongressComponent implements OnInit {

    constructor(
    ) {}

    ngOnInit(): void {
    }
}
