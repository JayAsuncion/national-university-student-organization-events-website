import {NgModule} from '@angular/core';
import {PagesService} from './services/pages.service';
import {PageListComponent} from './components/page-list.component';
import {CommonModule} from '@angular/common';
import {PagesRoutes} from './pages.routes';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AlertModule} from 'ngx-bootstrap/alert';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {PageItemComponent} from './components/page-item.component';
import {AboutUsComponent} from './components/templates/about-us.component';
import {ContactUsComponent} from './components/templates/contact-us.component';
import {NusgStudentCongressComponent} from './components/templates/nusg-student-congress.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagesRoutes,
        FontAwesomeModule,
        AlertModule.forRoot(),
        PaginationModule.forRoot(),
        FormsModule
    ],
    declarations: [
        AboutUsComponent,
        ContactUsComponent,
        NusgStudentCongressComponent,
        PageListComponent,
        PageItemComponent
    ],
    providers: [
        PagesService
    ],
    exports: [
        AboutUsComponent,
        PageListComponent,
        PageItemComponent
    ]
})
export class PagesModule {}
