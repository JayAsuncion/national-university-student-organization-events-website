import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageListComponent} from './components/page-list.component';
import {PageItemComponent} from './components/page-item.component';
import {AboutUsComponent} from './components/templates/about-us.component';

const routes: Routes = [
    {
        path: '',
        component: PageListComponent,
        data: {},
        runGuardsAndResolvers: 'always',
    },
    {
        path: ':page_category_code',
        component: PageListComponent,
        data: {},
        runGuardsAndResolvers: 'always',
    },
    {
        path: ':page_category_code/:id',
        component: PageItemComponent,
        data: {},
        runGuardsAndResolvers: 'always',
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutes {}
