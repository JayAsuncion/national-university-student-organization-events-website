import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class PagesService implements  OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    getPagesListDetails(limit = 5): Observable<any> {
        const url = this.resourceUrl + '/page-details?limit=' + limit;
        return this.httpClient.get(url);
    }

    getPagesListDetailsAndContent(optionsParam): Observable<any> {
        const options = {college_id: 1, page_category_code: '', event_category_id: 1, offset: 0, limit: 6, date_range: {start_date: '', end_date: ''}};
        options.page_category_code = (optionsParam.page_category_code !== undefined) ? optionsParam.page_category_code : 'event_page';
        options.event_category_id = (optionsParam.event_category_id !== undefined) ? optionsParam.event_category_id : 0;
        options.offset = (optionsParam.offset !== undefined) ? optionsParam.offset : 0;
        options.limit = (optionsParam.limit !== undefined) ? optionsParam.limit : 6;
        options.date_range.start_date = (optionsParam.date_range.start_date !== undefined) ? optionsParam.date_range.start_date : '';
        options.date_range.end_date = (optionsParam.date_range.end_date !== undefined) ? optionsParam.date_range.end_date : '';
        options.college_id = (optionsParam.college_id !== undefined) ? optionsParam.college_id : 1;

        const isStartDateSet = options.date_range.start_date.length > 0;
        const isEndDateSet = options.date_range.end_date.length > 0;

        let queryParams = '';

        if (isStartDateSet) {
            queryParams = queryParams + 'start_date=' + options.date_range.start_date;
        }

        if (isEndDateSet) {
            if (queryParams.length > 0) {
                queryParams = queryParams + '&';
            }

            queryParams = queryParams + 'end_date=' + options.date_range.end_date;
        }
        queryParams = (queryParams.length > 0) ? (queryParams + '&' + 'offset=' + options.offset) : ('offset=' + options.offset);
        queryParams = (queryParams.length > 0) ? (queryParams + '&' + 'limit=' + options.limit) : ('limit=' + options.limit);
        queryParams = (queryParams.length > 0) ? (queryParams + '&' + 'college_id=' + options.college_id) : ('college_id=' + options.college_id);
        queryParams = (queryParams.length > 0) ? (queryParams + '&' + 'page_category_code=' + options.page_category_code) : ('page_category_code=' + options.page_category_code);
        queryParams = (queryParams.length > 0) ? (queryParams + '&' + 'event_category_id=' + options.event_category_id) : ('event_category_id=' + options.event_category_id);

        const url = this.resourceUrl + '/pages?' + queryParams;
        return this.httpClient.get(url);
    }

    getPagesDetailsAndContent(id): Observable<any> {
        const url = this.resourceUrl + `/pages/${id}`;
        return this.httpClient.get(url);
    }
}
