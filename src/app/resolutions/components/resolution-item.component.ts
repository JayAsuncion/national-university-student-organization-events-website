import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ResolutionService} from '../services/resolution.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-resolution-item',
    templateUrl: './resolution-item.component.html',
    styleUrls: ['../../../assets/less/modules/resolutions/resolution-item.less'],
    encapsulation: ViewEncapsulation.None
})
export class ResolutionItemComponent implements OnInit {
    private destroyed$ = new Subject();

    public isResolutionItemLoaded = false;
    public isResolutionPDFLoaded = false;
    public resolutionItem;
    public resolutionTitle;

    constructor(
        private activatedRoute: ActivatedRoute,
        private resolutionService: ResolutionService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.resolutionTitle = localStorage.getItem('current_resolution');
        localStorage.removeItem('current_resolution');

        const resolutionID = this.activatedRoute.snapshot.paramMap.get('resolution_id');
        this.loadResolutionItem(resolutionID);
    }

    private loadResolutionItem(resolutionID) {
        this.resolutionService.fetchResolutionItem(resolutionID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.resolutionItem = response.data.resolution;
                        this.resolutionItem.resolution_url = JSON.parse(this.resolutionItem.resolution_url);
                        this.resolutionTitle = this.resolutionItem.resolution_title;
                        console.log(this.resolutionItem);
                        console.log('resolutionItem?.resolution_url[0]?.url', this.resolutionItem.resolution_url[0].url);
                    } else {
                        this.router.navigate(['/nusg-student-congress/resolutions']);
                    }

                    this.isResolutionItemLoaded = true;
                },
            error => {}
        );
    }
}
