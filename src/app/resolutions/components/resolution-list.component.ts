import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {ResolutionService} from '../services/resolution.service';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-resolution-list',
    templateUrl: './resolution-list.component.html',
    styleUrls: ['../../../assets/less/modules/resolutions/resolution-list.less'],
    encapsulation: ViewEncapsulation.None
})
export class ResolutionListComponent implements OnInit {
    private destroyed$ = new Subject();
    public siteURL = environment.app.site_url;

    public isResolutionListLoaded = false;
    public resolutionList = [];

    constructor(
        private resolutionService: ResolutionService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.getResolutionList();
    }

    private getResolutionList() {
        this.resolutionService.fetchResolutionList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.resolutionList = response.data.resolution_list;
                    console.log('resolutionList', this.resolutionList);
                }

                this.isResolutionListLoaded = true;
            },
            error => {
            }
        );
    }


    resolutionItemClickEvent(resolutionID, resolutionTitle) {
        localStorage.setItem('current_resolution', resolutionTitle);
        this.router.navigate([`/nusg-student-congress/resolutions/${resolutionID}`]);
    }
}
