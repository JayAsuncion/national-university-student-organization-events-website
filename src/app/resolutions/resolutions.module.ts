import {NgModule} from '@angular/core';
import {ResolutionsRoutes} from './resolutions.routes';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {ResolutionListComponent} from './components/resolution-list.component';
import {ResolutionItemComponent} from './components/resolution-item.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FontAwesomeModule,
        ResolutionsRoutes,
        PdfViewerModule
    ],
    declarations: [
        ResolutionListComponent,
        ResolutionItemComponent
    ],
    exports: [
    ],
    providers: [
    ]
})
export class ResolutionsModule {}
