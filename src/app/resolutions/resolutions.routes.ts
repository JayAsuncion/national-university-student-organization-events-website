import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResolutionListComponent} from './components/resolution-list.component';
import {ResolutionItemComponent} from './components/resolution-item.component';

const routes: Routes = [
    {
        path: '',
        component: ResolutionListComponent,
        data: {}
    },
    {
        path: ':resolution_id',
        component: ResolutionItemComponent,
        data: {}
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ResolutionsRoutes {}
