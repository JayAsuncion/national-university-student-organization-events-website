import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ResolutionService {
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    public fetchResolutionList(): Observable<any> {
        const url = this.resourceUrl + '/resolutions';
        return this.httpClient.get(url);
    }

    public fetchResolutionItem(resolutionID): Observable<any> {
        const url = this.resourceUrl + `/resolutions/${resolutionID}`;
        return this.httpClient.get(url);
    }
}
