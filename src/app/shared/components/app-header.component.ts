import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: 'app-header.component.html'
})

export class AppHeaderComponent implements OnInit {
    public isMobileLinksActive = false;

    ngOnInit(): void {
    }

    public toggleMobileLinks() {
        this.isMobileLinksActive = !this.isMobileLinksActive;
    }

    public navigationItemClickEvent(link) {
        this.toggleMobileLinks();
    }
}
