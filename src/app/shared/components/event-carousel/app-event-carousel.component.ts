import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {CarouselConfig} from 'ngx-bootstrap/carousel';
import {Router} from '@angular/router';

@Component({
    selector: 'app-event-carousel',
    templateUrl: './app-event-carousel.component.html',
    providers: [
        {provide: CarouselConfig, useValue: {interval: 3000, showIndicators: true}}
    ]
})
export class AppEventCarouselComponent implements OnInit {
    constructor(
        private router: Router
    ) {}

    @Input() set eventList(value) {
        const eventListCount = value.length;

        for (let i = 0; i < eventListCount; i++) {
            const pageContentsCount = (value[i].page_contents).length;

            for (let j = 0; j < pageContentsCount; j++) {
                value[i].page_contents[j].json_content = AppEventCarouselComponent.jsonDecode(
                    value[i].page_contents[j].json_content);
                value[i].page_contents[j].json_content.event_date = AppEventCarouselComponent.dateDecode(
                    value[i].page_contents[j].json_content.event_date);
            }
        }

        this.eventListData = value;
    }

    get eventList() {
        return this.eventListData;
    }
    private eventListData;

    public compCarouselConfig = {noPause: false};

    static jsonDecode(data) {
        return JSON.parse(data);
    }

    static dateDecode(isoDate: Date) {
        const decodedDate = new Date(isoDate);
        return decodedDate.toLocaleString();
    }

    ngOnInit(): void {

    }

    slideClick(pageID, pageName) {
        this.router.navigate([`/pages/event_page/${pageID}`]);
    }
}
