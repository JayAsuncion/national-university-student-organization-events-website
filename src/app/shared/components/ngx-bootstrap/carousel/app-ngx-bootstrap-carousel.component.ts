import {Component, Input} from '@angular/core';
import {CarouselConfig} from 'ngx-bootstrap/carousel';

@Component({
    selector: 'app-ngx-bootstrap-carousel',
    templateUrl: './app-ngx-bootstrap-carousel.component.html',
    providers: [
        {provide: CarouselConfig, useValue: {interval: 3000, showIndicators: true}}
    ]
})
export class AppNgxBootstrapCarouselComponent {
    public compCarouselConfig = {noPause: false};

    public carouselItems = [];

    @Input() set setCarouselItems(items) {
        console.log('setCarouselItems');
        if (typeof items !== 'undefined' && items.length > 0) {
            console.log('items', items);
            items.forEach((item) => {
               item.url = JSON.parse(item.url);
            });

            this.carouselItems = items;
            console.table(this.carouselItems);
        }
        this.carouselItems = items;
    }
}
