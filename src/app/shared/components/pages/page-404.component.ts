import {Component} from '@angular/core';

@Component({
    selector: 'app-page-404',
    templateUrl: './page-404.component.html',
    styleUrls: ['../../../../assets/less/components/pages/page404.less']
})
export class Page404Component {}
