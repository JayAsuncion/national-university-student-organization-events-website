import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AppHeaderComponent} from './components/app-header.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AppNgxBootstrapCarouselComponent} from './components/ngx-bootstrap/carousel/app-ngx-bootstrap-carousel.component';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {AppEventCarouselComponent} from './components/event-carousel/app-event-carousel.component';
import {Page404Component} from './components/pages/page-404.component';
import {SafeHtmlPipe} from './pipes/safe-html.pipe';
import {AppFooterComponent} from './components/app-footer.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule,
        CarouselModule.forRoot()
    ],
    declarations: [
        AppHeaderComponent,
        AppFooterComponent,
        AppEventCarouselComponent,
        AppNgxBootstrapCarouselComponent,
        Page404Component,
        SafeHtmlPipe
    ],
    exports: [
        AppHeaderComponent,
        AppFooterComponent,
        AppEventCarouselComponent,
        AppNgxBootstrapCarouselComponent,
        Page404Component,
        SafeHtmlPipe
    ]
})
export class SharedModule {}
