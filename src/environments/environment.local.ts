export const environment = {
    production: false,
    env: 'local',
    app: {
        site_url: 'http://local.officialnusg.com',
        api_url: 'http://local.api.officialnusg.com'
    }
};
