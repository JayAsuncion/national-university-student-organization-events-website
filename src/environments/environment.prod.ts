export const environment = {
    production: true,
    env: 'production',
    app: {
        site_url: 'https://officialnusg.com',
        api_url: 'https://api.officialnusg.com'
    }
};
