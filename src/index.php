<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>National University Student Government Official Website</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="assets/css_min/global-styles/global-styles.min.css?v=1.1.0">
    <script crossorigin="anonymous" src="assets/js/facebook-sdk.js"></script>
</head>
<body>
    <div id="fb-root"></div>
    <app-root></app-root>
</body>
</html>
